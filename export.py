from flask import Flask, request, jsonify
from flask_mail import Mail, Message

app = Flask(__name__)

# Configure Flask-Mail (Replace with your actual details)
app.config['MAIL_SERVER'] = 'smtp.example.com'
app.config['MAIL_PORT'] = 587
app.config['MAIL_USERNAME'] = 'your-email@example.com'
app.config['MAIL_PASSWORD'] = 'your-password'
app.config['MAIL_USE_TLS'] = True
app.config['MAIL_USE_SSL'] = False

mail = Mail(app)

@app.route('/send-pdf', methods=['POST'])
def send_pdf():
    email = request.form['email']
    pdf = request.files['pdf']

    msg = Message('Your PDF', sender='your-email@example.com', recipients=[email])
    msg.body = 'Please find the attached PDF.'
    msg.attach('saved-items.pdf', 'application/pdf', pdf.read())

    mail.send(msg)

    return jsonify(message='Email sent successfully') 

if __name__ == '__main__':
    app.run(debug=True)
