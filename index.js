/*
Filter Info animation
 */

const filterOpenBtn = document.querySelector('.filter-open-btn')
const filterCloseBtn = document.querySelector('.filter-close-btn')
const filter = document.querySelectorAll('.filter')

filterIsOpen = false;

filterOpenBtn.addEventListener('click', () => {
    if (filterIsOpen)
    {
        filter.forEach(filter_el => filter_el.classList.remove('visible'));
        filterIsOpen = false;
    }
    else
    {
        filter.forEach(filter_el => filter_el.classList.add('visible'));
        filterIsOpen = true;
    }
})

filterCloseBtn.addEventListener('click', () => {
    filter.forEach(filter_el => filter_el.classList.remove('visible'))
})

/*
Data Cluster Info Close and Open
*/

const closeDataClusterBtns = document.querySelectorAll('.data-cluster-info-close-btn')
const dataCluster = document.querySelectorAll('.data-cluster-info')

closeDataClusterBtns.forEach(closeDataClusterBtn => closeDataClusterBtn.addEventListener('click', () => {
    dataCluster.forEach(dataCluster => dataCluster.classList.remove('visible'));
}));



/*
Data Point Info Close and Open
*/

const closeDataPointBtns = document.querySelectorAll('.data-point-info-close-btn')
const dataPoints = document.querySelectorAll('.data-point-info')

closeDataPointBtns.forEach(closeDataPointBtn => closeDataPointBtn.addEventListener('click', () => {
    dataPoints.forEach(dataPoint => dataPoint.classList.remove('visible'));
}));


/*
Bookmarks bar close and open
*/
let savedItemsBarIsOpen = false;
const savedItemsToggle = document.getElementById("toggle-saved-items-panel");
const savedItemsBar = document.getElementById("saved-items-bar");
const savedItemsCloseBtn = document.querySelector('.saved-items-close-btn')

savedItemsToggle.addEventListener("click", (e)=> {
    if (savedItemsBarIsOpen) {
        savedItemsBar.classList.remove("visible");
        savedItemsBarIsOpen = false;
    } else {
        document.querySelector(".data-point-info.job-info").classList.remove("visible");
        document.querySelector(".data-point-info.alumni-info").classList.remove("visible");
        document.querySelector(".data-cluster-info.job-info").classList.remove("visible");

        savedItemsBar.classList.add("visible");
        savedItemsBarIsOpen = true;
    }
});

savedItemsCloseBtn.addEventListener("click", (e) => {
    if (savedItemsBarIsOpen)
    {
        savedItemsBar.classList.remove("visible");
        savedItemsBarIsOpen = false;
    }
})

let popupTimer;

document.addEventListener('DOMContentLoaded', function() {
    function showPopup() {
        document.getElementById('survey-popup').style.display = 'flex';
        // Clear any existing timer when showing the popup
        clearTimeout(popupTimer);
    }

    popupTimer = setTimeout(showPopup, 300000); // 300000 milliseconds = 5 minutes

    document.getElementById('survey-cancel').addEventListener('click', function() {
        document.getElementById('survey-popup').style.display = 'none';
        // Reset the timer when 'Cancel' is clicked
        popupTimer = setTimeout(showPopup, 300000);
    });
});




document.querySelector('.export-saved-items-button').addEventListener('click', function(event) {
    event.preventDefault();

    const { jsPDF } = window.jspdf;
    const doc = new jsPDF();

    let items = document.querySelectorAll('.saved-items-list li');
    items.forEach((item, index) => {
        doc.text(20, 10 + (10 * index), item.textContent);
    });

    let pdfBlob = doc.output('blob');
    let email = document.querySelector('.saved-items-email-input').value;

    let formData = new FormData();
    formData.append('email', email);
    formData.append('pdf', pdfBlob);

    fetch('/send-pdf', {
        method: 'POST',
        body: formData
    })
    .then(response => response.json())
    .then(data => {
        alert('Success: ' + data.message);
    })
    .catch((error) => {
        console.error('Error:', error);
        alert('Error sending email');
    });
});

